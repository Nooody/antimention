# AntiMention

[![Discord](https://img.shields.io/badge/Discord-Rejoindre-7289DA.svg)](https://discord.gg/mushway)
[![MiseAJour](https://img.shields.io/badge/%C3%80%20jour%20%3F-Oui-brightgreen.svg)](https://gitlab.com/Nooody/antimention)

Préface
-------

Avant toute chose, je vous recommande de prendre une des releases dans https://gitlab.com/Nooody/antimention/tags.

Si malgré tout vous êtes un dangereux développeur, vous pouvez toujours cloner le projet dans sa branche `develop`. Mais faites attention, il y a de grandes chances que le bot soit __très__ instable.

À propos des branches, il en existe 3 :
* La branche `master`, qui contient le code à jour, __testé__ (1) et soumis à une release
* La branche `hotfix`, qui contient aussi un code à jour, et qui peut corriger un bug qui vient d'être remarqué dans la branche `master`
* La branche `develop`, qui contient le __tout__ dernier code en __développement__, et qui peut contenir __beaucoup__ de bugs. Ce code reste en test, et __n'est pas complètement testé__.

## Setup 

Voir SETUP.md

Références
----------

(1) : La branche `master` peut contenir des bug, qui peuvent-être corrigés dans la branche `hotfix` s'il sont simple à corriger ou `develop`, s'il est nécessaire de les corriger pour en corriger un autre et s'il sont compliqué à corriger.
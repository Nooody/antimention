const Discord = require("discord.js");
const fs = require("fs");
const ms = require("ms");
let warns = JSON.parse(fs.readFileSync("./warnings.json", "utf8"));

module.exports.run = async (bot, message, args) => {

  //!warn @daeshan <reason>
  if(!message.member.hasPermission("MANAGE_MESSAGES")) return message.reply("Tu n'a pas la permission!");
  let wUser = message.guild.member(message.mentions.users.first()) || message.guild.members.get(args[0])
  if(!wUser) return message.reply("Utilisateur introuvable.");
  if(wUser.hasPermission("MANAGE_MESSAGES")) return message.reply("Tu n'a pas la permission!");
  let reason = args.join(" ").slice(22);

  if(!warns[wUser.id]) warns[wUser.id] = {
    warns: 0
  };

  warns[wUser.id].warns++;

  fs.writeFile("./warnings.json", JSON.stringify(warns), (err) => {
    if (err) console.log(err)
  });

  let sicon = "https://assets.gitlab-static.net/uploads/-/system/project/avatar/9117863/avatar.png";
  let warnEmbed = new Discord.RichEmbed()
  .setDescription("Avertissement : Réussi")
  .setColor("#FC8000")
  .setThumbnail(sicon)
  .addField("Utilisateur Warn :", `<@${wUser.id}>`)
  .addField("Nombre de Warns :", warns[wUser.id].warns)
  .addField("Raison :", reason);

  let warnchannel = message.guild.channels.find(`name`, "warns");
  if(!warnchannel) return message.reply("Couldn't find channel");

  warnchannel.send(warnEmbed);

  if(warns[wUser.id].warns == 3){
    let muterole = message.guild.roles.find(`name`, "muted");
    if(!muterole) return message.reply("Tu as besoin de créer un rôle.");

    let mutetime = "3h";
    await(wUser.addRole(muterole.id));
    message.channel.send(`<@${wUser.id}> As était temporairement muté`);;

    setTimeout(function(){
      wUser.removeRole(muterole.id)
      message.reply(`<@${wUser.id}> viens d'être unmuté.`)
    }, ms(mutetime))
  }
  if(warns[wUser.id].warns == 3){
    message.guild.member(wUser).ban(reason);
    message.reply(`<@${wUser.id}> viens d'être banni.`)
  }

}

module.exports.help = {
  name: "warn"
}

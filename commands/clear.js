const Discord = require("discord.js");

module.exports.run = async (bot, message, args) => {

  if(!message.member.hasPermission("MANAGE_MESSAGES")) return;
  if(!args[0]) return message.author.send("Merci de préciser un nombre.");
  message.channel.bulkDelete(args[0]).then(() => {
  message.channel.send(`${args[0]} messages supprimés.`).then(msg => msg.delete(2000));
});

}

module.exports.help = {
  name: "clear"
}

const settings = require("./settings.json");
const Discord = require("discord.js");
const superagent = require("superagent");
const YTDL = require("ytdl-core");
const fs = require("fs");
const bot = new Discord.Client({disableEveryone: true});
bot.commands = new Discord.Collection();

fs.readdir("./commands/", (err, files) => {

  if(err) console.log(err);
  let jsfile = files.filter(f => f.split(".").pop() === "js");
  if(jsfile.length <= 0){
    console.log("Couldn't find commands.");
    return;
  }

  jsfile.forEach((f, i) =>{
    let props = require(`./commands/${f}`);
    console.log(`${f} chargé et on!`);
    bot.commands.set(props.help.name, props);
  });
});

bot.on('ready', () => {
  console.log(`Connected as ${bot.user.username}`);
  bot.user.setActivity('le serveur M&C', {
    type: 'WATCHING'
  });
})

bot.on('message', message => {
  if (message.isMentioned('424337242295631873')) {
    if (message.member.roles.some(r => ["Propriétaire", "Youtubeur/Streameur", "Ami", "Modérateur", "Administrateur"].includes(r.name))) {
      return
    } else {
      message.delete();
    }
  }
});

bot.on('message', message => {
  if (message.isMentioned('268494047084150785')) {
    if (message.member.roles.some(r => ["Propriétaire", "Youtubeur/Streameur", "Ami", "Administrateur", "Modérateur"].includes(r.name))) {
      return
    } else {
      message.delete();
    }
  }
});

bot.on('message', message => {
  if (message.isMentioned('224827365417680896')) {
    message.delete();
  }
});


bot.on("message", message => {
   var sender = message.author;
   var msg = message.content.toUpperCase();

   if (sender.id === "440169378630074368") {
     return;
   }

   if(msg.includes('NEVERSAYYES')) {
     message.delete()
   }
   if(msg.includes('NEVER')) {
     message.delete()
   }
});

bot.on("message", async message => {
  if(message.author.bot) return;
  if(message.channel.type === "dm") return;

  let prefix = settings.prefix;
  let messageArray = message.content.split(" ");
  let cmd = messageArray[0];
  let args = messageArray.slice(1);
  let commandfile = bot.commands.get(cmd.slice(prefix.length));
  if(commandfile) commandfile.run(bot,message,args);

});

bot.login(settings.token);

# Installation

Guide pour installer correctement le bot "AntiMention"

## Préface

Je ne suis pas responsable si vous perdez des données dans ce processus, ou si vous n'arrivez pas à installer correctement le bot.

## 1. Base

Pour commencer, il vous faudra installer [NodeJS](https://nodejs.org/fr/) 

> Documentation ici https://nodejs.org/en/download/package-manager/
>
> Vous aurez besoin des droits administrateurs ou root si vous souhaitez installer NodeJS

Ensuite, une fois NodeJS installé, il vous faudra installer les dépendances du bot.

Il vous suffira de taper dans un invite de commande la commande suivante :

`npm install`



## 2. Les paramètres du bot

Ensuite, il vous faudra mettre en place les "paramètres" du bot.

Ceux-ci fonctionnent via un document nommé "settings.json".

Ce document doit être consitué comme ceci :

`{ "token":"votre token","prefix":"votre prefix" }`

> Pour obtenir un token, vous devez disposer d'un utilisateur bot dans disponible ici : https://discordapp.com/developers/applications/
>
> Votre préfixe est ce qui va différencier une commande d'un mot, par exemple la commande `!ping` marchera, mais pas `ping`

Un document d'exemple est disponible à cette adresse : https://nooody.me/help/settings.example.json ou à l'adresse miroir http://bit.ly/json_example.
> Le lien miroir sert en cas de non-fonctionnement de l'adresse https://nooody.me
>
> Cette adresse miroir redirige vers un lien Google Drive



## 3. Configuration

Une fois cela effectué, vous devrez mettre en place les IDs des personnes qui doivent voir leurs mentions disparaître.
Pour cela, vous devrez changer les IDs présents sur ces lignes :

`if (message.isMentioned('ID ICI'))`

Pour modifier les rôles qui peuvent mentionner l'ID ci-dessus, il faudra modifier les lignes suivantes :

`if (message.member.roles.some(r => ["Role 1", "Role 2"].includes(r.name))) {`

Pour ce qui va être des messages à supprimer si ils contiennent un mot précis, vous devrez changer les lignes suivantes :

`if(msg.includes('MOT ICI'))`
